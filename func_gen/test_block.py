
from subprocess import check_call

import ctypes

from dgemm_inner import *

c_double = ctypes.c_double

import numpy as np

from time import time

def test_block_1():

    nrc = 8
    ncc = 8
    ncd = 8
    align = 8

    axpy1 = DGEMMInner(num_rows_c=nrc, num_columns_c=ncc, num_columns_d=ncd, align=align)
    with open('dgemm_block.ll', 'w') as fh:
        fh.write(str(axpy1.module_dgemm))
    with open('dgemm_block.c', 'w') as fh:
        fh.write(str(axpy1.cfunc_dgemm))    

    print(' '.join((
        'clang',
        '-o',
        'dgemm_block.so',
        '-shared',
        '-fPIC',
        'dgemm_block.c',
        'dgemm_block.ll',
        '-Ofast',
        '-march=native'
    )))

    check_call((
        'clang',
        '-o',
        'dgemm_block.so',
        '-shared',
        '-fPIC',
        'dgemm_block.c',
        'dgemm_block.ll',
        '-Ofast',
        '-march=native'
    ))
    

    
    lib = ctypes.cdll.LoadLibrary('./dgemm_block.so')

    rng = np.random.RandomState(seed=12345)
    C = [np.require(rng.uniform(size=(nrc,)), dtype=c_double, requirements='A') for cx in range(ncc)]
    D = [np.require(rng.uniform(size=(ncd,)), dtype=c_double, requirements='A') for cx in range(ncc)]
    
    E = [np.require(rng.uniform(size=(nrc,)), dtype=c_double, requirements='A') for cx in range(ncd)]

    rC = np.transpose(np.array(C))
    rD = np.array(D)
    rE = np.transpose(np.array(E))
    print(C)
    print(D)
    print(E)
    print('-----')
    print(rE)
    print('-----')
    
    #print(axpy1.module_dgemm)
    #print(axpy1.cfunc_dgemm)

    ptrs = [ix.ctypes.get_as_parameter() for ix in C] + \
        [ix.ctypes.get_as_parameter() for ix in D] + \
        [ix.ctypes.get_as_parameter() for ix in E]
    
    if align is not None:
        for ptx in ptrs:
            assert ptx.value % align == 0
    
    for pxi, px in enumerate(ptrs):
        print(pxi, hex(px.value), px.value % 64)
    
    t0 = time()
    lib['dgemm_block_inner'](*ptrs)
    t1 = time()

    oE = np.transpose(np.array(E))

    t2 = time()
    rE = np.matmul(rC, rD) + rE
    t3 = time()

    print(C)
    print(D)
    print(E)
    print('-----')
    print(rE)
    
    #print("llvm", t1 - t0, "numpy", t3 - t2)
    assert np.allclose(oE, rE)
    





