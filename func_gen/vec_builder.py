"""
Very hacky manner to add vector insert element capability to 
ir.IRBuilder.
"""
from llvmlite import ir
from llvmlite.ir.instructions import Instruction

class InsertElement(Instruction):
    def __init__(self, parent, agg, elem, indices, name=''):
        typ = agg.type
        try:
            for i in indices:
                typ = typ.elements[i]
        except (AttributeError, IndexError):
            raise TypeError("Can't index at %r in %s"
                            % (list(indices), agg.type))
        if elem.type != typ:
            raise TypeError("Can only insert %s at %r in %s: got %s"
                            % (typ, list(indices), agg.type, elem.type))
        super(InsertElement, self).__init__(parent, agg.type, "insertelement",
                                          [agg, elem], name=name)

        self.aggregate = agg
        self.value = elem
        self.indices = indices

    def descr(self, buf):
        indices = [str(i) for i in self.indices]

        buf.append("insertelement {0} {1}, {2} {3}, i32 {4} {5}\n".format(
                   self.aggregate.type,
                   self.aggregate.get_reference(),
                   self.value.type,
                   self.value.get_reference(),
                   ', '.join(indices),
                   self._stringify_metadata(leading_comma=True),
                   ))


class VecBuilder(ir.IRBuilder):
    """
    Adds just enough to the default builder to allow values to be
    splat into vectors using llvm ir "insertelement"
    """
    def insert_element(self, agg, value, idx, name=''):
        """
        """
        if not isinstance(idx, (tuple, list)):
            idx = [idx]
        instr = InsertElement(
            self.block, agg, value, idx, name=name)
        self._insert(instr)
        return instr


