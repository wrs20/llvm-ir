
from dgemm_inner import DGEMMInner

import os
from subprocess import check_call
import ctypes

class DGEMMLib(object):
    def __init__(self, num_rows_c=4, num_columns_c=6, num_columns_d=4, align=8):
        self.num_rows_c = num_rows_c
        self.num_rows_d = num_columns_c
        self.num_columns_c = num_columns_c
        self.num_columns_d = num_columns_d
        self.align = align        
    
        self._inner_block = DGEMMInner(num_rows_c, num_columns_c, num_columns_d, align)

        self._tmpdir = './'
        with open(os.path.join(self._tmpdir, 'dgemm_block.ll'), 'w') as fh:
            fh.write(str(self._inner_block.module_dgemm))

        self._src = self._generate()
        self._build()

        self._lib = ctypes.cdll.LoadLibrary('./DGEMM_lib.so')['dgemm']
    
    def __call__(self, C, D, E):
        self._lib(
            ctypes.c_int(C.shape[1]),
            ctypes.c_int(C.shape[0]),
            ctypes.c_int(D.shape[1]),
            C.ctypes.get_as_parameter(),
            D.ctypes.get_as_parameter(),
            E.ctypes.get_as_parameter()
        )

    def _generate(self):
        
        COLS_C = ',\n'.join([
            '&C[(col_c + {cx}) * num_rows_c + row_c]'.format(cx=str(cx)) \
                for cx in range(self.num_rows_d)
        ])

        COLS_E = ',\n'.join([
            '&E[(col_d + {dx}) * num_rows_c + row_c]'.format(dx=str(dx)) \
                for dx in range(self.num_columns_d)
        ])

        ROWS_D = ',\n'.join([
            '&D[(row_d + {dx}) * num_cols_d + col_d]'.format(dx=str(dx)) \
                for dx in range(self.num_rows_d)
        ])



        # could be done with cgen
        csrc = r"""
#define INT int
#define REAL double
{FUNC_DEFN}
int dgemm(
    const INT num_rows_c,
    const INT num_cols_c,
    const INT num_cols_d,
    const REAL * C,
    const REAL * D,
    REAL * E
)
{{
    const INT num_rows_d = num_cols_c;
    const INT num_rows_e = num_rows_c;
    const INT num_cols_e = num_cols_d;

    const INT num_row_blk_c = num_rows_c / {NRC};
    const INT num_col_blk_c = num_cols_c / {NCC};
    const INT num_col_blk_d = num_cols_d / {NCD};

    //peel loops
    const int row_c_end = num_row_blk_c * {NRC};
    const int col_c_end = num_col_blk_c * {NCC};
    const int col_d_end = num_col_blk_d * {NCD};


    // block part
//#pragma omp parallel for schedule(dynamic) collapse(1)

for(INT ccx=0 ; ccx<num_col_blk_c ; ccx++){{
for(INT rcx=0 ; rcx<num_row_blk_c ; rcx++){{
for(INT dcx=0 ; dcx<num_col_blk_d ; dcx++){{
                const INT col_c = ccx * {NCC};
                const INT row_d = col_c;
                const INT row_c = rcx * {NRC};
                const INT col_d = dcx * {NCD};
                
                // get pointers to pass to inner
                dgemm_block_inner(
{COLS_C},
{ROWS_D},
{COLS_E}
                );
            }}
        }}
    }}



//#pragma omp parallel for schedule(dynamic) collapse(2)
    for(INT rcx=row_c_end ; rcx<num_rows_c ; rcx++){{
        for(INT dcx=col_d_end ; dcx<num_cols_d ; dcx++){{
            for(INT ccx=col_c_end ; ccx<num_cols_c ; ccx++){{
                const INT col_c = ccx;
                const INT row_d = col_c;
                const INT row_c = rcx;
                const INT col_d = dcx;
                E[col_d * num_rows_c + row_c] += C[col_c * num_rows_c + row_c] * D[row_d * num_cols_d + col_d];
            }}
        }}
    }}

    return 0;
}}
        """.format(
            FUNC_DEFN=self._inner_block.inner_dec,
            NRC=self.num_rows_c,
            NCC=self.num_columns_c,
            NRD=self.num_rows_d,
            NCD=self.num_columns_d,
            COLS_C=COLS_C,
            ROWS_D=ROWS_D,
            COLS_E=COLS_E
        )

        #print('\n------------------------------')
        #print(csrc)
        #print('------------------------------')

        return csrc

    def _build(self):
        
        with open(os.path.join(self._tmpdir, 'dgemm_lib.c'), 'w') as fh:
            fh.write(str(self._src))

        #print(' '.join((
        #    'clang',
        #    '-o',
        #    'DGEMM_lib.so',
        #    '-shared',
        #    '-fPIC',
        #    'dgemm_block.c',
        #    'dgemm_block.ll',
        #    '-Ofast',
        #    '-march=native'
        #)))

        check_call((
            'clang',
            '-Wno-override-module',
            '-o',
            'DGEMM_lib.so',
            '-shared',
            '-fPIC',
            'dgemm_lib.c',
            'dgemm_block.ll',
            '-Ofast',
            '-march=native'
        ))

