
from subprocess import check_call
import ctypes
from dgemm_lib import DGEMMLib

c_double = ctypes.c_double

import numpy as np
from time import time
from itertools import product

def vary_block_size(nrc, ncc, ncd, n=1024):
    # test with matrices that are an integer multiple of the block size
    align = 8

    dgemm1 = DGEMMLib(num_rows_c=nrc, num_columns_c=ncc, num_columns_d=ncd, align=align)
    
    NRC = n
    NCC = n
    NCD = n

    C_rmajor = np.array(np.random.uniform(size=(NRC, NCC)), dtype=c_double)
    
    C = np.transpose(C_rmajor).copy()
    
    D = np.array(np.random.uniform(size=(NCC, NCD)), dtype=c_double)

    E = np.zeros((NCD, NRC), dtype=c_double)
    

    dgemm1(C, D, E)

    t0 = time()
    dgemm1(C, D, E)
    t1 = time()
    
    flop_count = 2 * (n*n*n)
    return flop_count / (t1 - t0)




if __name__ == '__main__':
    for bs in product(range(4,17,4), range(2,9,2), range(2,9,2)):
        rate = vary_block_size(*bs)
        print(bs, rate/(10.**9))



