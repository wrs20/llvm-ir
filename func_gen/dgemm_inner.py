
from llvmlite import ir
from llvmlite.ir.values import NamedValue
from llvmlite.ir.instructions import Instruction, Ret, ExtractValue
from llvmlite.ir.types import ArrayType

from vector_type import VectorType
from vec_builder import VecBuilder

import cgen

double_type = ir.DoubleType()
int_type = ir.IntType(32)



class DGEMMInner(object):
    def __init__(self, num_rows_c=4, num_columns_c=6, num_columns_d=4, align=8):
        self.num_rows_c = num_rows_c
        self.num_rows_d = num_columns_c
        self.num_columns_c = num_columns_c
        self.num_columns_d = num_columns_d
        self.align = align
        self._last_var = 0
        self.vector_type = VectorType(double_type, self.num_rows_c)
        self.array_type = ArrayType(double_type, self.num_columns_d)
        self.fma_intrinsic = 'llvm.fma.v{}f64'.format(num_rows_c)
        self._cgen_sig = ()
        self._cgen_call = ()
        self.module_dgemm = self._generate_dgemm()
        self.cfunc_dgemm = self._generate_wrapper()
        self.inner_dec = cgen.FunctionDeclaration(
            cgen.Value("int", "dgemm_block_inner"),
            self._cgen_sig)

    def get_next_var(self):
        self._last_var += 1
        return 'r' + str(self._last_var)
    
    def _generate_wrapper(self):
        func = ''
        func = cgen.Module(
            (
                cgen.FunctionDeclaration(cgen.Value("int", "dgemm_block_inner"),
                    self._cgen_sig),
                # cgen.Line('extern "C"'),
                cgen.FunctionBody(
                    cgen.FunctionDeclaration(cgen.Value("int", "dgemm_block"),
                        self._cgen_sig),
                    cgen.Block([
                        cgen.Statement(
                            'return dgemm_block_inner(' + (','.join(self._cgen_call)) + ')'
                        ),
                    ])
                ),
            )
        )
        return func

    def _generate_dgemm(self):

        #Create an empty module
        module = ir.Module(name="dgemm_inner {} {} {} {}".format(
            self.num_rows_c, self.num_rows_d, 
            self.num_columns_c, self.num_columns_d)
        )

        # add the vfma intrinsic
        fma_type = ir.FunctionType(self.vector_type,
                (self.vector_type, self.vector_type, self.vector_type))
        fma_func = ir.Function(module, fma_type, name=self.fma_intrinsic)


        # Order: inputs then outputs
        # columns of A as vector pointers
        # rows of B as array pointers
        # output columns in C as vector pointers
        fnty = ir.FunctionType(int_type,
            tuple(
                [
                    self.vector_type.as_pointer() for argx in range(
                    self.num_columns_c)
                ] + [
                    self.array_type.as_pointer() for argx in range(
                    self.num_rows_d)
                ] + [
                    self.vector_type.as_pointer() for argx in range(
                    self.num_columns_d)
                ]
            )
        )
        func = ir.Function(module, fnty, name="dgemm_block_inner")
        func.attributes.add("alwaysinline")
        
        # generate the cgen function args
        tmp_sig = []
        tmp_call = []
        for ax in range(self.num_columns_c):
            sym = self.get_next_var()
            tmp_sig.append(
                cgen.Const(cgen.Pointer(cgen.Value('double', sym)))
            )
            tmp_call.append(sym)
        for ax in range(self.num_rows_d):
            sym = self.get_next_var()
            tmp_sig.append(
                cgen.Const(cgen.Pointer(cgen.Value('double', sym)))
            )
            tmp_call.append(sym) 
        for ax in range(self.num_columns_d):
            sym = self.get_next_var()
            tmp_sig.append(
                cgen.Pointer(cgen.Value('double', sym))
            )
            tmp_call.append(sym) 



        self._cgen_sig = tmp_sig
        self._cgen_call = tmp_call


        a_inputs = func.args[: self.num_columns_c: ]
        tend = self.num_columns_c + self.num_rows_d
        b_inputs = func.args[self.num_columns_c: tend :]
        c_outputs = func.args[tend : tend+self.num_columns_d : ]
            
        block = func.append_basic_block(name="entry")
        # builder = ir.IRBuilder(block)
        builder = VecBuilder(block)

        a_loads = [builder.load(vx, align=self.align) for vx in a_inputs]
        b_loads = [builder.load(vx, align=self.align) for vx in b_inputs]
        c_loads = [builder.load(vx, align=self.align) for vx in c_outputs]
        
        # this list should be overwritten with the most recent
        # output registers at any given moment
        c_regs = [vx for vx in c_loads]

        for ccx in range(self.num_columns_c):
            for cdx in range(self.num_columns_d):
                # splat the coefficients into a vector
                tmp_coeff = builder.extract_value(b_loads[ccx], cdx)
                tmp_alloc_p = builder.alloca(self.vector_type, 1)
                tmp_alloc = builder.load(tmp_alloc_p, align=self.align)
                tmp_splat = tmp_alloc
                for tix in range(self.num_rows_c):
                    tmp_splat = builder.insert_element(tmp_splat, tmp_coeff, tix)
                assert tmp_splat is not None
                c_regs[cdx] = builder.call(fma_func, (a_loads[ccx], tmp_splat, c_regs[cdx]))


        c_stores = [builder.store(vx, px, align=self.align) for vx, px in zip(c_regs, c_outputs)] 
        builder.ret(int_type(0))

        return module





if __name__ == '__main__':
    axpy1 = DGEMMInner()
    print(axpy1.module_dgemm)
    with open('dgemm_block.ll', 'w') as fh:
        fh.write(str(axpy1.module_dgemm))

    with open('dgemm_block.c', 'w') as fh:
        fh.write(str(axpy1.cfunc_dgemm))

