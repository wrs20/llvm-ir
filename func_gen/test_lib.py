
from subprocess import check_call

import ctypes

from dgemm_lib import DGEMMLib

c_double = ctypes.c_double

import numpy as np

from time import time

def test_lib_1():
    # test with matrices that are an integer multiple of the block size
    nrc = 8
    ncc = 8
    ncd = 4
    align = 8

    dgemm1 = DGEMMLib(num_rows_c=nrc, num_columns_c=ncc, num_columns_d=ncd, align=align)
    
    for testx in range(20):
        NRC = np.random.randint(1, 20) * nrc
        NCC = np.random.randint(1, 20) * ncc
        NCD = np.random.randint(1, 20) * ncd

        C_rmajor = np.array(np.random.uniform(size=(NRC, NCC)), dtype=c_double)
        
        C = np.transpose(C_rmajor).copy()
        
        D = np.array(np.random.uniform(size=(NCC, NCD)), dtype=c_double)

        E = np.zeros((NCD, NRC), dtype=c_double)

        E_rmajor = np.matmul(C_rmajor, D)

        dgemm1(C, D, E)

        err = np.linalg.norm(np.transpose(E).ravel() - E_rmajor.ravel(), np.inf)

        assert err < 10.**-13, str(NRC) + ' ' + str(NCC) + ' ' + str(NCD)

def test_lib_2():
    # test with matrices that are not integer multiples of the blocks
    nrc = 2
    ncc = 2
    ncd = 2
    align = 8

    dgemm1 = DGEMMLib(num_rows_c=nrc, num_columns_c=ncc, num_columns_d=ncd, align=align)
    
    NRC = np.random.randint(1, 20) * nrc
    NCC = np.random.randint(1, 20) * ncc
    NCD = np.random.randint(1, 20) * ncd
    
    NRC = 1
    NCC = 1
    NCD = 3

    C_rmajor = np.array(np.random.uniform(size=(NRC, NCC)), dtype=c_double)
    
    C = np.transpose(C_rmajor).copy()
    
    D = np.array(np.random.uniform(size=(NCC, NCD)), dtype=c_double)

    E = np.zeros((NCD, NRC), dtype=c_double)

    E_rmajor = np.matmul(C_rmajor, D)
    
    print('CORRECT')
    print(E_rmajor)
    print('-'*60)

    dgemm1(C, D, E)

    print('C')
    print(np.transpose(E))
    print('-'*60)

    err = np.linalg.norm(np.transpose(E).ravel() - E_rmajor.ravel(), np.inf)
    print(err)
    #assert err < 10.**-13, str(NRC) + ' ' + str(NCC) + ' ' + str(NCD)







