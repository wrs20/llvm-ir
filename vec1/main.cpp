#include <stdio.h>

#define N 4

extern "C" int axpy(double a, double *x, double *y);

extern "C" int wrapper_axpy(double a, double *x, double *y){
    axpy(a,x,y);
    return 0;
}


#ifdef MAIN
int main(int argc, char * argv[]){
    
    double a = 1.2;
    double x[N] = {2.0, 2.0, 2.0, 2.0};
    double y[N] = {3.0, 3.0, 3.0, 3.0};

    axpy(a, x, y);
    
    for(int ix=0 ; ix<N ; ix++){
        printf("%f\t%f\n", x[ix], y[ix]);
    }

    return 0;
}
#endif
