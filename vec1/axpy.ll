
;declare <4 x double> @llvm.fma.4xf64(<4 x double>, <4 x double>, <4 x double>)
declare <4 x double> @"llvm.fma.4xf64"(<4 x double> %".1", <4 x double> %".2", <4 x double> %".3")

define i32 @"axpy"(double %a, <4 x double>* %xp, <4 x double>* %yp) alwaysinline {
    

    %x0 = load <4 x double>, <4 x double>* %xp
    %y0 = load <4 x double>, <4 x double>* %yp
    
    ; splat a into a vector
    %avs = alloca <4 x double>
    %avs0 = load <4 x double>, <4 x double> * %avs
    %av0 = insertelement <4 x double> %avs0, double %a, i32 0
    %av1 = insertelement <4 x double> %av0, double %a, i32 1
    %av2 = insertelement <4 x double> %av1, double %a, i32 2
    %av3 = insertelement <4 x double> %av2, double %a, i32 3
    
    %out = call <4 x double> @llvm.fma.4xf64(<4 x double> %av3, <4 x double> %x0, <4 x double> %y0)


    store <4 x double> %out, <4 x double>* %yp

    ret i32 0
}
